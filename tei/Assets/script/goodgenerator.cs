﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class goodgenerator : MonoBehaviour
{
    public GameObject good;
    float span = 0;
    float delta = 0;
    void Start()
    {
        
    }
    // Update is called once per frame
    void Update()
    {
        span = Random.Range(-0.3f, 100.0f);
        delta = 0;
        this.delta += Time.deltaTime;
        if (this.delta > this.span)
        {
            this.delta = 0;
            GameObject go = Instantiate(good) as GameObject;
            int px = Random.Range(-4, 4);
            go.transform.position = new Vector3(10, px, 0);
        }
    }
}
