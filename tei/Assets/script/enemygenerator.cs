﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemygenerator : MonoBehaviour
{
    public GameObject enemy;
    float span = 0.7f;
    float delta = 0f;
    // Update is called once per frame
    void Update()
    {
        this.delta += Time.deltaTime;
        if(this.delta> this.span)
        {
            this.delta = 0;
            GameObject go = Instantiate(enemy) as GameObject;
            int px = Random.Range(-4, 4);
            go.transform.position = new Vector3(10, px, 0);
        }
    }
}
